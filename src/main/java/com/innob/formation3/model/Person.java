package com.innob.formation3.model;

import com.innob.formation3.helper.models.Gender;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "persons")
public class Person {

    private Long id;
    private String name;
    private String firstName;
    private int age;
    private Gender gender;
    private Date birthDate;
   // private List<Person> friends;

    public Person() {

    }

    public Person(Long id, String name, String firstName, int age, Gender gender, Date birthDate) { //List<Person> friends) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.age = age;
        this.gender = gender;
        this.birthDate = birthDate;
    //    this.friends = friends;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "first_name", nullable = false)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "age", nullable = false)
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Column(name = "gender", nullable = false)
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Column(name = "birth_date", nullable = true)
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
/*
    @ElementCollection
    @JoinTable(name = "friends")
    public List<Person> getFriends() {
        return friends;
    }

    public void setFriends(List<Person> friends) {
        this.friends = friends;
    }
*/
    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", birthDate=" + birthDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return getAge() == person.getAge() &&
                getId().equals(person.getId()) &&
                Objects.equals(getName(), person.getName()) &&
                Objects.equals(getFirstName(), person.getFirstName()) &&
                getGender() == person.getGender() &&
                Objects.equals(birthDate, person.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getFirstName(), getAge(), getGender(), birthDate);
    }
}