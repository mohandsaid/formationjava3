package com.innob.formation3;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

/*import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Stream.of;
import static org.apache.commons.lang3.ArrayUtils.add;

@SpringBootApplication
@RestController
@ImportResource(value = {"classpath:/app-context.xml"})
public class Application {
    private static Pattern CONFIG_PATTERN = Pattern.compile("--config-location=(.*)");*/


/*    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Application.class);
        app.run(getConfigLocation(args));
    }*/

/*    private static String[] getConfigLocation(String[] args) {
        StringBuilder config = new StringBuilder("--spring.config.location=classpath:/application.properties,");
        of(args).map(CONFIG_PATTERN::matcher).filter(Matcher::find).map(m -> m.group(1)).findFirst().ifPresent(config::append);
        return add(args, config.toString());
    }
}*/