package com.innob.formation3.helper.utils;

import com.innob.formation3.helper.mapper.MapToPerson;
import com.innob.formation3.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static java.util.stream.Collectors.toSet;


//@Component
public class ImportCSV {

    static Logger log = LoggerFactory.getLogger(ImportCSV.class);

    private final static String UNDER_SCORE = "_";
    private final static String PATH_INPUT = "/home/innob/travail/project/data/in/";
    private final static String PATH_OUTPUT = "/home/innob/travail/project/data/out/";
    private final static String NAME_FILE = "persons";
    private final static String EXTENSION_FILE = ".csv";
    private final static String CSV_FILE = PATH_INPUT + NAME_FILE + EXTENSION_FILE;

     public static Set<Person> readCsvFile(String file) {
         return getPeople(file);
     }

    public static Set<Person> getPeople(String file) {
        Set<Person> personsSet = null;
        try {
            personsSet = Files.lines(Paths.get(file)).skip(1).map(MapToPerson::mapToPerson).collect(toSet());
        } catch (IOException e) {
            log.error("Error: File {} is Not Found !", file);
        }
        return personsSet;
    }

}