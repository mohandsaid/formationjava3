package com.innob.formation3.helper.mapper;

import com.innob.formation3.model.Person;

import java.util.ArrayList;
import java.util.List;

public class MapToList {
    private static final String COMMA = ",";

    public static List<Person> mapToList(String stringList) {
        String[] friendId = stringList.split(COMMA);
        Person element = new Person();
        List<Person> response = new ArrayList<>();
        for (String id : friendId) {
            element.setId(Long.valueOf(id.replaceAll("\\s+","")));
            response.add(element);
        }
        return response;
    }
}
