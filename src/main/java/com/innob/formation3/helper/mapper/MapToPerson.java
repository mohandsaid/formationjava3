package com.innob.formation3.helper.mapper;

import com.innob.formation3.helper.models.Gender;
import com.innob.formation3.helper.utils.DateUtils;
import com.innob.formation3.model.Person;

import java.text.ParseException;

import static com.innob.formation3.helper.mapper.MapToList.*;

public class MapToPerson {

    private static final String SEMICOLON = ";";

    // TODO: Ajouter les autres cloumnes une fois le code fonctionne.

    public static Person mapToPerson(String ligne) {
        String[] p = ligne.split(SEMICOLON);
        Person element = new Person();
        try {
            element.setId(Long.valueOf(p[0]));
            element.setName(p[1]);
            element.setFirstName(p[2]);
            element.setGender(Gender.valueOf(p[3].replaceAll("\\s+","")));
            element.setAge(Integer.parseInt(p[4].replaceAll("\\s+",""))); //delete whitespaces
            element.setBirthDate(DateUtils.getDate(p[5].replaceAll("\\s+", "")));
          //  element.setFriends(mapToList(p[6]));
        }catch (ParseException e){
            e.getMessage();
        }
        return element;
    }
}
