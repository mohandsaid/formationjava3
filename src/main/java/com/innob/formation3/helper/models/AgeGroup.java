package com.innob.formation3.helper.models;

public enum AgeGroup {

    UNDER_18,
    BETWEEN_18_AND_40,
    BETWEEN_41_AND_60,
    OVER_60;
}
