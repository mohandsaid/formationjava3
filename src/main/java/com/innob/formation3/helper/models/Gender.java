package com.innob.formation3.helper.models;

public enum Gender {

    MALE("homme"),
    FEMALE("femme");
    private String value ;

    Gender(String value) {
        this.value = value;
    }

}